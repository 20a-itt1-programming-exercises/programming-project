"""

Exercixe 4
Assume that we execute the following assignment statements:

width = 17
height = 12.0

1. width//2
17//2 = 8 and 1 as a rest, type - int

2. width/2.0
17/2.0 = 8.5 , type - float

3. height/3
12.0/3 = 4, type - float (but you can use int)

4. 1 + 2 * 5
1 + 2 * 5 = 11, type - int

"""
ex1 = 17//2
print (ex1)

ex2 = 17/2.0
print (ex2)

ex3 = 12.0/3
print (ex3)

ex4 = 1 + 2 * 5
print (ex4)