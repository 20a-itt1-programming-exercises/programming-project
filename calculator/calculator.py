def addTwo(numb1, numb2):
    sumadd = numb1 + numb2
    return sumadd


def subTwo(numb1, numb2):
    sumsub = numb1 - numb2
    return sumsub


def divTwo(numb1, numb2):
    sumdiv = numb1 / numb2
    return sumdiv


def mulTwo(numb1, numb2):
    summul = numb1 * numb2
    return summul


try:
    while True:

        numb1 = input("Enter first number: ")
        numb2 = input("Enter second number: ")
        symb = input(
            "Choose action by pressing number: \n 1 add \n 2 sub \n 3 div \n 4 mul \n")

        if numb1 == "done" or numb2 == "done" or symb == "done":
            print("Done!")
            break

        numb1 = int(numb1)
        numb2 = int(numb2)
        symb = int(symb)

        if symb == 1:
            sum1 = addTwo(numb1, numb2)
            print("Your answer: "+str(sum1))
            break

        if symb == 2:
            sum2 = subTwo(numb1, numb2)
            print("Your answer: " + str(sum2))
            break

        if symb == 3:
            sum3 = divTwo(numb1, numb2)
            print("Your answer: " + str(sum3))
            break

        if symb == 4:
            sum4 = mulTwo(numb1, numb2)
            print("Your answer: " + str(sum4))
            break

    print("Goodbye!")

except ZeroDivisionError:
    print("Division by zero illegal!")
except Exception:
    print("Just numbers and done...")