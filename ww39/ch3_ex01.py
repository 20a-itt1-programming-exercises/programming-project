hours = float(input("Enter hours: "))
rate = float(input("Enter rate: "))

if hours > 40:
    sum1 = ((hours - 40) * rate * 1.5) + rate * 40
    print("Pay: " + str(sum1))

elif 0 < hours <= 40:
    sum = hours * rate
    print("Pay: " + str(sum))

elif hours <= 0 or rate <= 0:
    print("Wrong data")
