"""

1.14 exercises

Exercise 1:
What is the function of the secondary memory in a computer?
- Store information for the long term, even beyond a power cycle.

Exercise 2:
What is a program?
- Program is a logical sequance of actions which executes tasks.

Exercise 3:
What is the difference between a compiler and an interpreter?
- Interpreter runs slowly, starts right away and lets see results.
Compiler needs extra preparation time, but then program runs very quickly.

Exercise 4:
Which of the following contains "machine code"?
- The Python interpreter.

Exercise 5:
What is wrong with the following code?
- Syntax error, 'primt' instead of 'print'.

Exercise 6:
Where in the computer is a variable such as "x" stored after the following Python line finishes?
- Main memory.

Exercise 7:
What will the following program print out:
- 44

Exercise 8:
Explain each of the following using an example of a human capability:
(1) Central processing unit - is considered as the brain of the computer, performs all types of data processing operations.
(2) Main memory - RAM - is where programs and data are kept when the processor is actively using them.
(3) Secondary memory - is where programs and data are kept on a long-term basis.
(4) Input device - is used to bring data into the system (keyboard, mouse, microphone..).
(5) Output device - is used to send data out of the system (monitor, printer, speaker).

Exercise 9:
How do you fix a "Syntax Error"?
- Check the spelling of the code. 
"""